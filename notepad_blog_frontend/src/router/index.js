import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home')
  }, {
    path: '/register',
    name: 'register',
    component: () => import('@/views/auth/Register')
  }, {
    path: '/404',
    name: '404',
    component: () => import('@/views/error/404'),
    meta: { title: '404-Notfound' }
  }, {
    path: '*',
    redirect: '404',
    hidden: true
  }, {
    path: '/login',
    name: 'login',
    component: () => import('@/views/auth/login'),
    meta: { title: '登录' }
  },
  // 发布
  {
    name: "post-create",
    path: "/post/create",
    component: () => import("@/views/post/Create"),
    meta: { title: "发布文章", requireAuth: true },
  },
  // 文章详情
  {
    name: "post-detail",
    path: "/post/:id",
    component: () => import("@/views/post/Detail"),
    meta: { title: "详情" },
  },
  // 文章修改编辑
  {
    name: 'topic-edit',
    path: '/topic/edit/:id',
    component: () => import('@/views/post/Edit'),
    meta: {
      title: '编辑',
      requireAuth: true
    }
  },
  {
    name: 'tag',
    path: '/tag/:name',
    component: () => import('@/views/tag/Tag'),
    meta: { title: '标签' }
  },
  // 搜索
  {
    name: 'search',
    path: '/search',
    component: () => import('@/views/Search'),
    meta: { title: '搜索' }
  },
  // 用户主页
  {
    name: 'user',
    path: '/member/:username/home',
    component: () => import('@/views/user/Profile'),
    meta: { title: '用户中心', requireAuth: true}
  },
  // 设置中心
  {
    name: 'user-setting',
    path: '/member/:username/setting',
    component: () => import('@/views/user/Setting'),
    meta: { title: '设置',requireAuth: true }
  },
  // 标签页
  {
    name: 'tag',
    path: '/tag',
    component: () => import('@/views/tag/TagList'),
    meta: { title: '标签'}
  }

]


const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};


const router = new VueRouter({
  routes
})

export default router
