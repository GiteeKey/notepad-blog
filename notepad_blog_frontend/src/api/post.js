  
import request from '@/utils/request'

// 列表
export function getList(pageNo, size, tab) {
  return request(({
    url: '/post/list',
    method: 'get',
    params: { pageNo: pageNo, size: size, tab: tab }
  }))
}

// 发布文章
export function post(topic) {
  return request({
    url: '/post/create',
    method: 'post',
    data: topic
  })
}

// 获取文章详情
export function getTopic(id) {
  return request({
    url: `/post`,
    method: 'get',
    params: {
      id: id
    }
  })
}

// 随便看看(推荐)
export function getRecommendTopics(id) {
  return request({
    url: '/post/recommend',
    method: 'get',
    params: {
      topicId: id
    }
  })
}

// 更新博客
export function update(topic) {
  return request({
    url: '/post/update',
    method: 'post',
    data: topic
  })
}

// 根据id删除博客
export function deleteTopic(id) {
  return request({
    url: `/post/delete/${id}`,
    method: 'delete'
  })
}