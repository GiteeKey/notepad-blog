import request from '@/utils/request'

// 首页点击标签
export function getTopicsByTag(paramMap) {
  return request({
    url: '/tag/' + paramMap.name,
    method: 'get',
    params: {
      page: paramMap.page,
      size: paramMap.size
    }
  })
}

// 标签页
export function getPostTag(paramMap) {
  return request({
    url: 'tag/list/' + paramMap.name,
    method: 'get',
    params: {
      page: paramMap.page,
      size: paramMap.size
    }
  })
}