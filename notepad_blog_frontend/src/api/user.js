import request from '@/utils/request'

// 用户主页(个人中心)
export function getInfoByName(username, page, size) {
  return request({
    url: '/auth/user/' + username,
    method: 'get',
    params: {
      pageNo: page,
      size: size
    }
  })
}

// 用户主页（设置中心）
export function getInfo() {
  return request({
    url: '/auth/user/info',
    method: 'get'
  })
}
// 更新
export function update(user) {
  return request({
    url: '/auth/user/update',
    method: 'post',
    data: user
  })
}
