import request from '@/utils/request'

// 注册
export function userRegister(UserDTO) {
    return request({
        url: '/auth/user/register',
        method: 'post',
        data: UserDTO
    })
}

// 登录
export function login(data) {
    return request({
      url: '/auth/user/login',
      method: 'post',
      data
    })
  }

// 获取用户信息
export function getUserInfo() {
  return request({
    url: '/auth/user/info',
    method: 'get'
  })
}

// 注销
export function logout() {
  return request({
    url: '/auth/user/logout',
    method: 'get'
  })
}

