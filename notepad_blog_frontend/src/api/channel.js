import request from '@/utils/request'

export function getChannel() {
    return request({
        url: '/channel/list',
        method: 'get'
    })
}