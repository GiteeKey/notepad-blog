/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1:3306
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 127.0.0.1:3306
 Source Schema         : nodepad_mblog

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 14/02/2021 23:34:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bms_billboard
-- ----------------------------
DROP TABLE IF EXISTS `bms_billboard`;
CREATE TABLE `bms_billboard`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '公告时间',
  `show` tinyint(1) NULL DEFAULT NULL COMMENT '1：展示中，0：过期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '全站公告' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_billboard
-- ----------------------------
INSERT INTO `bms_billboard` VALUES (2, 'R1.0 开始已实现护眼模式 ,妈妈再也不用担心我的眼睛了。', '2020-11-19 17:16:19', 1);
INSERT INTO `bms_billboard` VALUES (4, '系统已更新至最新版1.0.1', '2021-02-11 19:38:42', 1);

-- ----------------------------
-- Table structure for bms_channel
-- ----------------------------
DROP TABLE IF EXISTS `bms_channel`;
CREATE TABLE `bms_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(5) NOT NULL,
  `thumbnail` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of bms_channel
-- ----------------------------
INSERT INTO `bms_channel` VALUES (1, 'banner', 'banner', 1, '', 0);
INSERT INTO `bms_channel` VALUES (2, '后端', '后端', 0, '', 100);
INSERT INTO `bms_channel` VALUES (3, '前端', '前端', 1, '', 90);
INSERT INTO `bms_channel` VALUES (4, '数据库', '数据库', 0, '', 80);
INSERT INTO `bms_channel` VALUES (5, 'Linux', 'Linux', 0, NULL, 70);
INSERT INTO `bms_channel` VALUES (6, 'ElasticSearch', 'ElasticSearch', 1, '', 60);
INSERT INTO `bms_channel` VALUES (7, '其他', '其他', 0, NULL, 50);

-- ----------------------------
-- Table structure for bms_comment
-- ----------------------------
DROP TABLE IF EXISTS `bms_comment`;
CREATE TABLE `bms_comment`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作者ID',
  `post_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'topic_id',
  `create_time` datetime(0) NOT NULL COMMENT '发布时间',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_comment
-- ----------------------------
INSERT INTO `bms_comment` VALUES ('1', 'hun', '1349290158897311745', '1360554911123570690', '2021-02-13 21:38:46', NULL);
INSERT INTO `bms_comment` VALUES ('1360596499912626177', 'enen', '1360072208037122049', '1360554911123570690', '2021-02-13 22:27:39', NULL);
INSERT INTO `bms_comment` VALUES ('1360596762484445186', 'zhangsan', '1360072208037122049', '1360554701039271938', '2021-02-13 22:28:41', NULL);
INSERT INTO `bms_comment` VALUES ('1360596813759811586', 'lis', '1360072208037122049', '1360554701039271938', '2021-02-13 22:28:54', NULL);
INSERT INTO `bms_comment` VALUES ('1360778034523631618', 'd', '1360072208037122049', '1349362401438392322', '2021-02-14 10:29:00', NULL);
INSERT INTO `bms_comment` VALUES ('1360779324251140097', '1', '1360778252610662401', '1360554052729901057', '2021-02-14 10:34:08', NULL);
INSERT INTO `bms_comment` VALUES ('1360780053477027842', '1111', '1360778252610662401', '1360554052729901057', '2021-02-14 10:37:01', NULL);
INSERT INTO `bms_comment` VALUES ('1360780189246648321', '1111', '1360778252610662401', '1360554052729901057', '2021-02-14 10:37:34', NULL);
INSERT INTO `bms_comment` VALUES ('1360780231349071873', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:37:44', NULL);
INSERT INTO `bms_comment` VALUES ('1360780747550453762', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:39:47', NULL);
INSERT INTO `bms_comment` VALUES ('1360780873434091522', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:40:17', NULL);
INSERT INTO `bms_comment` VALUES ('1360781003881140225', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:40:48', NULL);
INSERT INTO `bms_comment` VALUES ('1360781332886540290', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:42:07', NULL);
INSERT INTO `bms_comment` VALUES ('1360781459403526145', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:42:37', NULL);
INSERT INTO `bms_comment` VALUES ('1360782035847696386', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:44:54', NULL);
INSERT INTO `bms_comment` VALUES ('1360782847713955842', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:48:08', NULL);
INSERT INTO `bms_comment` VALUES ('1360782944799510530', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:48:31', NULL);
INSERT INTO `bms_comment` VALUES ('1360783050433056770', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:48:56', NULL);
INSERT INTO `bms_comment` VALUES ('1360783058318348290', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:48:58', NULL);
INSERT INTO `bms_comment` VALUES ('1360783306918891521', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:49:57', NULL);
INSERT INTO `bms_comment` VALUES ('1360783406785269762', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:50:21', NULL);
INSERT INTO `bms_comment` VALUES ('1360783860671877121', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:52:09', NULL);
INSERT INTO `bms_comment` VALUES ('1360783960324345857', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:52:33', NULL);
INSERT INTO `bms_comment` VALUES ('1360784351107649537', '', '1360778252610662401', '1360554052729901057', '2021-02-14 10:54:06', NULL);
INSERT INTO `bms_comment` VALUES ('1360784976574885890', 'cc', '1360778252610662401', '1360554052729901057', '2021-02-14 10:56:35', NULL);

-- ----------------------------
-- Table structure for bms_follow
-- ----------------------------
DROP TABLE IF EXISTS `bms_follow`;
CREATE TABLE `bms_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '被关注人ID',
  `follower_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关注人ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户关注' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_follow
-- ----------------------------
INSERT INTO `bms_follow` VALUES (65, '1329723594994229250', '1317498859501797378');
INSERT INTO `bms_follow` VALUES (85, '1332912847614083073', '1332636310897664002');
INSERT INTO `bms_follow` VALUES (129, '1349290158897311745', '1349618748226658305');

-- ----------------------------
-- Table structure for bms_post
-- ----------------------------
DROP TABLE IF EXISTS `bms_post`;
CREATE TABLE `bms_post`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'markdown内容',
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作者ID',
  `comments` int(11) NOT NULL DEFAULT 0 COMMENT '评论统计',
  `collects` int(11) NOT NULL DEFAULT 0 COMMENT '收藏统计',
  `view` int(11) NOT NULL DEFAULT 0 COMMENT '浏览统计',
  `top` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否置顶，1-是，0-否',
  `essence` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否加精，1-是，0-否',
  `section_id` int(11) NULL DEFAULT 0 COMMENT '专栏ID',
  `create_time` datetime(0) NOT NULL COMMENT '发布时间',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '话题表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_post
-- ----------------------------
INSERT INTO `bms_post` VALUES ('1332650453142827009', '哈哈哈，helloworld', '这是第一篇哦\n\n> hi :handshake: 你好\n\n`hello world`\n\n:+1: 很好\n', '1349290158897311745', 0, 0, 32, b'0', b'0', 1, '2020-11-28 19:40:02', '2020-11-28 19:46:39', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1332681213400817665', '聚合查询并统计', '* [x] SQL：\n\n```sql\nSELECT s.*,\nCOUNT(t.id) AS topics\nFROM section s\nLEFT JOIN topic t\nON s.id = t.section_id\nGROUP BY s.title\n```\n\n', '1349290158897311745', 0, 0, 59, b'0', b'0', 1, '2020-11-28 21:42:16', '2020-11-29 15:00:42', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1332682473151635458', '我要发财', '2021 冲冲冲！！！\n\n', '1349290158897311745', 0, 0, 94, b'0', b'0', 2, '2020-11-28 21:47:16', '2020-11-30 19:40:22', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1333432347031646209', '哈哈哈，换了个dark主题', '主题更换为Dark\n\n', '1349290158897311745', 0, 0, 9, b'0', b'0', 0, '2020-11-30 23:27:00', NULL, 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1333447953558765569', '1', '12\n2\n\n', '1349290158897311745', 0, 0, 77, b'0', b'0', 0, '2020-12-01 00:29:01', '2020-12-03 23:56:51', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1333668258587750401', '嘿嘿，测试一下啊', '大家好\n`Hello everyone!`\n\n\n\n', '1349290158897311745', 0, 0, 9, b'0', b'0', 0, '2020-12-01 15:04:26', '2020-12-01 16:49:14', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1333676096156528641', '测试', '测试\n\n', '1349290158897311745', 0, 0, 38, b'0', b'0', 0, '2020-12-01 15:35:34', NULL, 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1333695976536748034', '最新版本介绍，同步更新！', '<p align=center>一款基于SpringBoot构建的智慧社区系统</p>\n\n<p align=center>\n<a href=\"https://github.com/1020317774/rhapsody-admin/stargazers\"><img alt=\"GitHub release\" src=\"https://img.shields.io/github/release/1020317774/rhapsody-admin?style=flat-square\"></a>\n<a href=\"https://github.com/1020317774/rhapsody-admin/blob/main/LICENSE\"><img alt=\"GitHub license\" src=\"https://img.shields.io/github/license/1020317774/rhapsody-admin\"></a>\n</p>\n\n### Hi there :wave:\n\n<!--\n**1020317774/1020317774** is a :sparkles: _special_ :sparkles: repository because its `README.md` (this file) appears on your GitHub profile.\n\nHere are some ideas to get you started:\n\n- :telescope: I’m currently working on ...\n- :seedling: I’m currently learning ...\n- :dancers: I’m looking to collaborate on ...\n- :thinking: I’m looking for help with ...\n- :speech_balloon: Ask me about ...\n- :mailbox: How to reach me: ...\n- :smile: Pronouns: ...\n- :zap: Fun fact: ...\n-->\n\n[![1020317774\'s github stats](https://github-readme-stats.vercel.app/api?username=1020317774&show_icons=true&count_private=true)](https://github.com/1020317774)\n\n[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=1020317774&layout=compact)](https://github.com/1020317774)\n---------\n\n> 作者：王一晨\n> github：[https://github.com/1020317774](https://github.com/1020317774)\n\n## 技术栈\n\n- [x] SpringBoot 2.X\n- [x] Mysql 8.X\n- [x] Mybatis\n- [x] MybatisPlus\n- [x] Redis\n- [x] Jwt\n- [x] FastJson\n- [x] Hutool\n- [x] Lombok\n- [ ] ElasticSearch\n\n……\n\n## 安装指导\n\n- 克隆\n\n```java\ngit clone https://github.com/1020317774/rhapsody-admin.git\n```\n\n- 修改`application.properties`选择环境\n- 修改多环境配置中的redis参数和数据库\n- 启动`BootApplication`\n- 访问[`http://127.0.0.1:10000`](http://127.0.0.1:10000)\n\n', '1349290158897311745', 0, 0, 45, b'1', b'1', 0, '2020-12-01 16:54:34', '2020-12-01 17:05:00', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1334481725322297346', 'hello，spring-security', ':hibiscus: spring-security\n\n', '1349290158897311745', 0, 0, 49, b'0', b'0', 0, '2020-12-03 20:56:51', NULL, 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1335149981733449729', '视频嵌入', ':+1:\n\n[https://www.bilibili.com/video/BV1w64y1f7w3](https://www.bilibili.com/video/BV1w64y1f7w3)\n\n[1](https://www.bilibili.com/video/BV1tp4y1x72w)\n\n```\n.vditor-reset pre > code\n```\n\n```\npublic class HelloWorld {\n\npublic static void main(String[] args) {\n    System.out.println(\"Hello World!\");\n}\n}\n```\n\n', '1349290158897311745', 0, 0, 47, b'0', b'0', 0, '2020-12-05 17:12:16', '2021-01-14 13:06:16', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1349362401438392322', '2021 健康，快乐', '2021的`FLAG`\n\n1. 技能进步\n2. 没有烦恼\n3. 发财 :smile:\n\n', '1349290158897311745', 0, 0, 23, b'0', b'0', 0, '2021-01-13 22:27:21', '2021-01-14 17:30:13', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1349631541260595202', '权限部分 OK', '1. 创建 ok\n2. 修改 ok\n3. 删除 ok\n\n', '1349290158897311745', 0, 0, 17, b'0', b'0', 0, '2021-01-14 16:16:49', '2021-01-14 16:18:53', 'wenzhangmiaoshu');
INSERT INTO `bms_post` VALUES ('1360513129052577794', '888', '88\n88888\n77\n\n', '1360072208037122049', 0, 0, 0, b'0', b'0', 0, '2021-02-13 16:56:22', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360513154323259393', '888', '88\n88888\n77\n\n', '1360072208037122049', 0, 0, 2, b'0', b'0', 0, '2021-02-13 16:56:28', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360514056601935873', '8', '8888888\n\n', '1360072208037122049', 0, 0, 0, b'0', b'0', 0, '2021-02-13 17:00:03', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360548757836288002', '888', '888\n\n', '1360072208037122049', 0, 0, 1, b'0', b'0', 0, '2021-02-13 19:17:56', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360550062604570625', '测试', '## 文章发表前端(vditor)\n\n### 1.安装vditor组件\n\n```\nyarn add vditor\n```\n\n### 2.api\n\n**src\\api\\post.js中添加**\n\n```\n// 发布\nexport function post(topic) {\n  return request({\n    url: \'/post/create\',\n    method: \'post\',\n    data: topic\n  })\n}\n```\n\n### 3.路由\n\n**src\\router\\index.js中添加**\n\n```\n// 发布\n  {\n    name: \"post-create\",\n    path: \"/post/create\",\n    component: () => import(\"@/views/post/Create\"),\n    meta: { title: \"信息发布\", requireAuth: true },\n  }\n```\n\n### 4.新增Create页面\n\n**src\\views\\post\\创建Create.vue**\n\n```\n<template>\n  <div class=\"columns\">\n    <div class=\"column is-full\">\n      <el-card\n        class=\"box-card\"\n        shadow=\"never\"\n      >\n        <div\n          slot=\"header\"\n          class=\"clearfix\"\n        >\n          <span><i class=\"fa fa fa-book\"> 主题 / 发布主题</i></span>\n        </div>\n        <div>\n          <el-form\n            ref=\"ruleForm\"\n            :model=\"ruleForm\"\n            :rules=\"rules\"\n            class=\"demo-ruleForm\"\n          >\n            <el-form-item prop=\"title\">\n              <el-input\n                v-model=\"ruleForm.title\"\n                placeholder=\"输入主题名称\"\n              />\n            </el-form-item>\n​\n            <!--Markdown-->\n            <div id=\"vditor\" />\n​\n            <b-taginput\n              v-model=\"ruleForm.tags\"\n              class=\"my-3\"\n              maxlength=\"15\"\n              maxtags=\"3\"\n              ellipsis\n              placeholder=\"请输入主题标签，限制为 15 个字符和 3 个标签\"\n            />\n​\n            <el-form-item>\n              <el-button\n                type=\"primary\"\n                @click=\"submitForm(\'ruleForm\')\"\n              >立即创建\n              </el-button>\n              <el-button @click=\"resetForm(\'ruleForm\')\">重置</el-button>\n            </el-form-item>\n          </el-form>\n        </div>\n      </el-card>\n    </div>\n  </div>\n</template>\n​\n<script>\nimport { post } from \'@/api/post\'\nimport Vditor from \'vditor\'\nimport \'vditor/dist/index.css\'\nexport default {\n  name: \'TopicPost\',\n  data() {\n    return {\n      contentEditor: {},\n      ruleForm: {\n        title: \'\', // 标题\n        tags: [], // 标签\n        content: \'\' // 内容\n      },\n      rules: {\n        title: [\n          { required: true, message: \'请输入话题名称\', trigger: \'blur\' },\n          {\n            min: 1,\n            max: 25,\n            message: \'长度在 1 到 25 个字符\',\n            trigger: \'blur\'\n          }\n        ]\n      }\n    }\n  },\n  mounted() {\n    this.contentEditor = new Vditor(\'vditor\', {\n      height: 500,\n      placeholder: \'此处为话题内容……\',\n      theme: \'classic\',\n      counter: {\n        enable: true,\n        type: \'markdown\'\n      },\n      preview: {\n        delay: 0,\n        hljs: {\n          style: \'monokai\',\n          lineNumber: true\n        }\n      },\n      tab: \'\\t\',\n      typewriterMode: true,\n      toolbarConfig: {\n        pin: true\n      },\n      cache: {\n        enable: false\n      },\n      mode: \'sv\'\n    })\n  },\n  methods: {\n    submitForm(formName) {\n      this.$refs[formName].validate((valid) => {\n        if (valid) {\n          if (\n            this.contentEditor.getValue().length === 1 ||\n            this.contentEditor.getValue() == null ||\n            this.contentEditor.getValue() === \'\'\n          ) {\n            alert(\'话题内容不可为空\')\n            return false\n          }\n          if (this.ruleForm.tags == null || this.ruleForm.tags.length === 0) {\n            alert(\'标签不可以为空\')\n            return false\n          }\n          this.ruleForm.content = this.contentEditor.getValue()\n          post(this.ruleForm).then((response) => {\n            const { data } = response\n            setTimeout(() => {\n              this.$router.push({\n                name: \'post-detail\',\n                params: { id: data.id }\n              })\n            }, 800)\n          })\n        } else {\n          console.log(\'error submit!!\')\n          return false\n        }\n      })\n    },\n    resetForm(formName) {\n      this.$refs[formName].resetFields()\n      this.contentEditor.setValue(\'\')\n      this.ruleForm.tags = \'\'\n    }\n  }\n}\n</script>\n​\n<style>\n</style>\n```\n\n### 5.测试页面\n\n![image-20210213153111929](https://gitee.com/GiteeKey/mblog/raw/master/images/image-20210213153111929.png)\n\n## 文章发表后端\n\n### DTO\n\n```\nimport lombok.Data;\nimport java.io.Serializable;\nimport java.util.List;\n​\n@Data\npublic class CreatePostDTO implements Serializable {\n    private static final long serialVersionUID = -5957433707110390852L;\n​\n    /**\n     * 标题\n     */\n    private String title;\n​\n    /**\n     * 内容\n     */\n    private String content;\n​\n    /**\n     * 标签\n     */\n    private List<String> tags;\n​\n}\n```\n\n### BmsPostController\n\n```\n/**\n * 创建文章\n *\n * @return\n */\n@PostMapping(\"/create\")\npublic ApiResult<Page<PostVO>> createPost(\n        @RequestHeader(value = \"userName\") String userName,\n        @RequestBody CreatePostDTO createPostDTO) {\n    postService.createPost(userName, createPostDTO);\n    return ApiResult.success(null,\"保存成功\");\n}\n```\n\n### BmsPostService\n\n```\n/**\n * 保存文章\n * @param userName\n * @param createPostDTO\n */\n@Transactional(rollbackFor = RuntimeException.class)\npublic void createPost(String userName, CreatePostDTO createPostDTO) {\n    \n    // 获取用户\n    UmsUser loginUser = userService.getOne(\n            new LambdaQueryWrapper<UmsUser>().eq(UmsUser::getUsername, userName)\n    );\n    if (ObjectUtils.isNull(loginUser)) {\n        ApiAsserts.fail(\"用户不存在\");\n    }\n    \n    // 保存\n    BmsPost post = BmsPost.builder()\n            .userId(loginUser.getId())\n            .title(createPostDTO.getTitle())\n            .content(createPostDTO.getContent())\n            .createTime(new Date())\n            .build();\n    this.save(post);\n    // 给用户增加积分+1\n    userService.updateById(loginUser.setScore(loginUser.getScore() + 1));\n    \n    // 保存标签(先查询是否存在)\n    for (String tag : createPostDTO.getTags()) {\n        BmsTag bmsTag = tagMapper.selectOne(\n                new LambdaQueryWrapper<BmsTag>().eq(BmsTag::getName, tag)\n        );\n        //如果不存在，保存\n        if (ObjectUtils.isNull(bmsTag)){\n            bmsTag = new BmsTag();\n            bmsTag.setName(tag);\n            bmsTag.setPostCount(1);\n            tagMapper.insert(bmsTag);\n            System.out.println(\"bmsTag = \" + bmsTag);\n        }else{// 否存 + 1\n            tagMapper.updateById(bmsTag.setPostCount(bmsTag.getPostCount()+1));\n        }\n        // 保存post_tag\n        System.out.println(\"post.getId() = \" + post.getId());\n        System.out.println(\"bmsTag.getId() = \" + bmsTag.getId());\n        postTagService.save(BmsPostTag.builder().postId(post.getId()).tagId(bmsTag.getId()).build());\n        \n    }\n}\n```\n\n## 文章详情前端\n\n### 1.src\\api\\修改post.js\n\n```\n// 获取文章详情\nexport function getTopic(id) {\n  return request({\n    url: `/post`,\n    method: \'get\',\n    params: {\n      id: id\n    }\n  })\n}\n```\n\n### 2.路由\n\n```\n// 文章详情\n  {\n    name: \"post-detail\",\n    path: \"/post/:id\",\n    component: () => import(\"@/views/post/Detail\"),\n    meta: { title: \"详情\" },\n  }\n```\n\n### 3.src\\views\\post\\创建Detail.vue\n\n```\n<template>\n  <div class=\"columns\">\n    <!--文章详情-->\n    <div class=\"column is-three-quarters\">\n      <!--主题-->\n      <el-card\n        class=\"box-card\"\n        shadow=\"never\"\n      >\n        <div\n          slot=\"header\"\n          class=\"has-text-centered\"\n        >\n          <p class=\"is-size-5 has-text-weight-bold\">{{ topic.title }}</p>\n          <div class=\"has-text-grey is-size-7 mt-3\">\n            <span>{{ dayjs(topic.createTime).format(\'YYYY/MM/DD HH:mm:ss\') }}</span>\n            <el-divider direction=\"vertical\" />\n            <span>发布者：{{ topicUser.alias }}</span>\n            <el-divider direction=\"vertical\" />\n            <span>查看：{{ topic.view }}</span>\n          </div>\n        </div>\n​\n        <!--Markdown-->\n        <div id=\"preview\" />\n​\n        <!--标签-->\n        <nav class=\"level has-text-grey is-size-7 mt-6\">\n          <div class=\"level-left\">\n            <p class=\"level-item\">\n              <b-taglist>\n                <router-link\n                  v-for=\"(tag, index) in tags\"\n                  :key=\"index\"\n                  :to=\"{ name: \'tag\', params: { name: tag.name } }\"\n                >\n                  <b-tag type=\"is-info is-light mr-1\">\n                    {{ \"#\" + tag.name }}\n                  </b-tag>\n                </router-link>\n              </b-taglist>\n            </p>\n          </div>\n          <div\n            v-if=\"token && user.id === topicUser.id\"\n            class=\"level-right\"\n          >\n            <router-link\n              class=\"level-item\"\n              :to=\"{name:\'topic-edit\',params: {id:topic.id}}\"\n            >\n              <span class=\"tag\">编辑</span>\n            </router-link>\n            <a class=\"level-item\">\n              <span\n                class=\"tag\"\n                @click=\"handleDelete(topic.id)\"\n              >删除</span>\n            </a>\n          </div>\n        </nav>\n      </el-card>\n​\n    </div>\n​\n    <div class=\"column\">\n      作者信息\n    </div>\n  </div>\n</template>\n​\n<script>\nimport { deleteTopic, getTopic } from \'@/api/post\'\nimport { mapGetters } from \'vuex\'\nimport Vditor from \'vditor\'\nimport \'vditor/dist/index.css\'\nexport default {\n  name: \'TopicDetail\',\n  computed: {\n    ...mapGetters([\n      \'token\',\'user\'\n    ])\n  },\n  data() {\n    return {\n      flag: false,\n      topic: {\n        content: \'\',\n        id: this.$route.params.id\n      },\n      tags: [],\n      topicUser: {}\n    }\n  },\n  mounted() {\n    this.fetchTopic()\n  },\n  methods: {\n    renderMarkdown(md) {\n      Vditor.preview(document.getElementById(\'preview\'), md, {\n        hljs: { style: \'github\' }\n      })\n    },\n    // 初始化\n    async fetchTopic() {\n      getTopic(this.$route.params.id).then(response => {\n        const { data } = response\n        document.title = data.topic.title\n        this.topic = data.topic\n        this.tags = data.tags\n        this.topicUser = data.user\n        // this.comments = data.comments\n        this.renderMarkdown(this.topic.content)\n        this.flag = true\n      })\n    },\n    handleDelete(id) {\n      deleteTopic(id).then(value => {\n        const { code, message } = value\n        alert(message)\n        if (code === 200) {\n          setTimeout(() => {\n            this.$router.push({ path: \'/\' })\n          }, 500)\n        }\n      })\n    }\n  }\n}\n</script>\n​\n<style>\n#preview {\n  min-height: 300px;\n}\n</style>\n```\n\n', '1360072208037122049', 0, 0, 7, b'0', b'0', 0, '2021-02-13 19:23:07', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360550099032100866', '测试', '## 文章发表前端(vditor)\n\n### 1.安装vditor组件\n\n```\nyarn add vditor\n```\n\n### 2.api\n\n**src\\api\\post.js中添加**\n\n```\n// 发布\nexport function post(topic) {\n  return request({\n    url: \'/post/create\',\n    method: \'post\',\n    data: topic\n  })\n}\n```\n\n### 3.路由\n\n**src\\router\\index.js中添加**\n\n```\n// 发布\n  {\n    name: \"post-create\",\n    path: \"/post/create\",\n    component: () => import(\"@/views/post/Create\"),\n    meta: { title: \"信息发布\", requireAuth: true },\n  }\n```\n\n### 4.新增Create页面\n\n**src\\views\\post\\创建Create.vue**\n\n```\n<template>\n  <div class=\"columns\">\n    <div class=\"column is-full\">\n      <el-card\n        class=\"box-card\"\n        shadow=\"never\"\n      >\n        <div\n          slot=\"header\"\n          class=\"clearfix\"\n        >\n          <span><i class=\"fa fa fa-book\"> 主题 / 发布主题</i></span>\n        </div>\n        <div>\n          <el-form\n            ref=\"ruleForm\"\n            :model=\"ruleForm\"\n            :rules=\"rules\"\n            class=\"demo-ruleForm\"\n          >\n            <el-form-item prop=\"title\">\n              <el-input\n                v-model=\"ruleForm.title\"\n                placeholder=\"输入主题名称\"\n              />\n            </el-form-item>\n​\n            <!--Markdown-->\n            <div id=\"vditor\" />\n​\n            <b-taginput\n              v-model=\"ruleForm.tags\"\n              class=\"my-3\"\n              maxlength=\"15\"\n              maxtags=\"3\"\n              ellipsis\n              placeholder=\"请输入主题标签，限制为 15 个字符和 3 个标签\"\n            />\n​\n            <el-form-item>\n              <el-button\n                type=\"primary\"\n                @click=\"submitForm(\'ruleForm\')\"\n              >立即创建\n              </el-button>\n              <el-button @click=\"resetForm(\'ruleForm\')\">重置</el-button>\n            </el-form-item>\n          </el-form>\n        </div>\n      </el-card>\n    </div>\n  </div>\n</template>\n​\n<script>\nimport { post } from \'@/api/post\'\nimport Vditor from \'vditor\'\nimport \'vditor/dist/index.css\'\nexport default {\n  name: \'TopicPost\',\n  data() {\n    return {\n      contentEditor: {},\n      ruleForm: {\n        title: \'\', // 标题\n        tags: [], // 标签\n        content: \'\' // 内容\n      },\n      rules: {\n        title: [\n          { required: true, message: \'请输入话题名称\', trigger: \'blur\' },\n          {\n            min: 1,\n            max: 25,\n            message: \'长度在 1 到 25 个字符\',\n            trigger: \'blur\'\n          }\n        ]\n      }\n    }\n  },\n  mounted() {\n    this.contentEditor = new Vditor(\'vditor\', {\n      height: 500,\n      placeholder: \'此处为话题内容……\',\n      theme: \'classic\',\n      counter: {\n        enable: true,\n        type: \'markdown\'\n      },\n      preview: {\n        delay: 0,\n        hljs: {\n          style: \'monokai\',\n          lineNumber: true\n        }\n      },\n      tab: \'\\t\',\n      typewriterMode: true,\n      toolbarConfig: {\n        pin: true\n      },\n      cache: {\n        enable: false\n      },\n      mode: \'sv\'\n    })\n  },\n  methods: {\n    submitForm(formName) {\n      this.$refs[formName].validate((valid) => {\n        if (valid) {\n          if (\n            this.contentEditor.getValue().length === 1 ||\n            this.contentEditor.getValue() == null ||\n            this.contentEditor.getValue() === \'\'\n          ) {\n            alert(\'话题内容不可为空\')\n            return false\n          }\n          if (this.ruleForm.tags == null || this.ruleForm.tags.length === 0) {\n            alert(\'标签不可以为空\')\n            return false\n          }\n          this.ruleForm.content = this.contentEditor.getValue()\n          post(this.ruleForm).then((response) => {\n            const { data } = response\n            setTimeout(() => {\n              this.$router.push({\n                name: \'post-detail\',\n                params: { id: data.id }\n              })\n            }, 800)\n          })\n        } else {\n          console.log(\'error submit!!\')\n          return false\n        }\n      })\n    },\n    resetForm(formName) {\n      this.$refs[formName].resetFields()\n      this.contentEditor.setValue(\'\')\n      this.ruleForm.tags = \'\'\n    }\n  }\n}\n</script>\n​\n<style>\n</style>\n```\n\n### 5.测试页面\n\n![image-20210213153111929](https://gitee.com/GiteeKey/mblog/raw/master/images/image-20210213153111929.png)\n\n## 文章发表后端\n\n### DTO\n\n```\nimport lombok.Data;\nimport java.io.Serializable;\nimport java.util.List;\n​\n@Data\npublic class CreatePostDTO implements Serializable {\n    private static final long serialVersionUID = -5957433707110390852L;\n​\n    /**\n     * 标题\n     */\n    private String title;\n​\n    /**\n     * 内容\n     */\n    private String content;\n​\n    /**\n     * 标签\n     */\n    private List<String> tags;\n​\n}\n```\n\n### BmsPostController\n\n```\n/**\n * 创建文章\n *\n * @return\n */\n@PostMapping(\"/create\")\npublic ApiResult<Page<PostVO>> createPost(\n        @RequestHeader(value = \"userName\") String userName,\n        @RequestBody CreatePostDTO createPostDTO) {\n    postService.createPost(userName, createPostDTO);\n    return ApiResult.success(null,\"保存成功\");\n}\n```\n\n### BmsPostService\n\n```\n/**\n * 保存文章\n * @param userName\n * @param createPostDTO\n */\n@Transactional(rollbackFor = RuntimeException.class)\npublic void createPost(String userName, CreatePostDTO createPostDTO) {\n    \n    // 获取用户\n    UmsUser loginUser = userService.getOne(\n            new LambdaQueryWrapper<UmsUser>().eq(UmsUser::getUsername, userName)\n    );\n    if (ObjectUtils.isNull(loginUser)) {\n        ApiAsserts.fail(\"用户不存在\");\n    }\n    \n    // 保存\n    BmsPost post = BmsPost.builder()\n            .userId(loginUser.getId())\n            .title(createPostDTO.getTitle())\n            .content(createPostDTO.getContent())\n            .createTime(new Date())\n            .build();\n    this.save(post);\n    // 给用户增加积分+1\n    userService.updateById(loginUser.setScore(loginUser.getScore() + 1));\n    \n    // 保存标签(先查询是否存在)\n    for (String tag : createPostDTO.getTags()) {\n        BmsTag bmsTag = tagMapper.selectOne(\n                new LambdaQueryWrapper<BmsTag>().eq(BmsTag::getName, tag)\n        );\n        //如果不存在，保存\n        if (ObjectUtils.isNull(bmsTag)){\n            bmsTag = new BmsTag();\n            bmsTag.setName(tag);\n            bmsTag.setPostCount(1);\n            tagMapper.insert(bmsTag);\n            System.out.println(\"bmsTag = \" + bmsTag);\n        }else{// 否存 + 1\n            tagMapper.updateById(bmsTag.setPostCount(bmsTag.getPostCount()+1));\n        }\n        // 保存post_tag\n        System.out.println(\"post.getId() = \" + post.getId());\n        System.out.println(\"bmsTag.getId() = \" + bmsTag.getId());\n        postTagService.save(BmsPostTag.builder().postId(post.getId()).tagId(bmsTag.getId()).build());\n        \n    }\n}\n```\n\n## 文章详情前端\n\n### 1.src\\api\\修改post.js\n\n```\n// 获取文章详情\nexport function getTopic(id) {\n  return request({\n    url: `/post`,\n    method: \'get\',\n    params: {\n      id: id\n    }\n  })\n}\n```\n\n### 2.路由\n\n```\n// 文章详情\n  {\n    name: \"post-detail\",\n    path: \"/post/:id\",\n    component: () => import(\"@/views/post/Detail\"),\n    meta: { title: \"详情\" },\n  }\n```\n\n### 3.src\\views\\post\\创建Detail.vue\n\n```\n<template>\n  <div class=\"columns\">\n    <!--文章详情-->\n    <div class=\"column is-three-quarters\">\n      <!--主题-->\n      <el-card\n        class=\"box-card\"\n        shadow=\"never\"\n      >\n        <div\n          slot=\"header\"\n          class=\"has-text-centered\"\n        >\n          <p class=\"is-size-5 has-text-weight-bold\">{{ topic.title }}</p>\n          <div class=\"has-text-grey is-size-7 mt-3\">\n            <span>{{ dayjs(topic.createTime).format(\'YYYY/MM/DD HH:mm:ss\') }}</span>\n            <el-divider direction=\"vertical\" />\n            <span>发布者：{{ topicUser.alias }}</span>\n            <el-divider direction=\"vertical\" />\n            <span>查看：{{ topic.view }}</span>\n          </div>\n        </div>\n​\n        <!--Markdown-->\n        <div id=\"preview\" />\n​\n        <!--标签-->\n        <nav class=\"level has-text-grey is-size-7 mt-6\">\n          <div class=\"level-left\">\n            <p class=\"level-item\">\n              <b-taglist>\n                <router-link\n                  v-for=\"(tag, index) in tags\"\n                  :key=\"index\"\n                  :to=\"{ name: \'tag\', params: { name: tag.name } }\"\n                >\n                  <b-tag type=\"is-info is-light mr-1\">\n                    {{ \"#\" + tag.name }}\n                  </b-tag>\n                </router-link>\n              </b-taglist>\n            </p>\n          </div>\n          <div\n            v-if=\"token && user.id === topicUser.id\"\n            class=\"level-right\"\n          >\n            <router-link\n              class=\"level-item\"\n              :to=\"{name:\'topic-edit\',params: {id:topic.id}}\"\n            >\n              <span class=\"tag\">编辑</span>\n            </router-link>\n            <a class=\"level-item\">\n              <span\n                class=\"tag\"\n                @click=\"handleDelete(topic.id)\"\n              >删除</span>\n            </a>\n          </div>\n        </nav>\n      </el-card>\n​\n    </div>\n​\n    <div class=\"column\">\n      作者信息\n    </div>\n  </div>\n</template>\n​\n<script>\nimport { deleteTopic, getTopic } from \'@/api/post\'\nimport { mapGetters } from \'vuex\'\nimport Vditor from \'vditor\'\nimport \'vditor/dist/index.css\'\nexport default {\n  name: \'TopicDetail\',\n  computed: {\n    ...mapGetters([\n      \'token\',\'user\'\n    ])\n  },\n  data() {\n    return {\n      flag: false,\n      topic: {\n        content: \'\',\n        id: this.$route.params.id\n      },\n      tags: [],\n      topicUser: {}\n    }\n  },\n  mounted() {\n    this.fetchTopic()\n  },\n  methods: {\n    renderMarkdown(md) {\n      Vditor.preview(document.getElementById(\'preview\'), md, {\n        hljs: { style: \'github\' }\n      })\n    },\n    // 初始化\n    async fetchTopic() {\n      getTopic(this.$route.params.id).then(response => {\n        const { data } = response\n        document.title = data.topic.title\n        this.topic = data.topic\n        this.tags = data.tags\n        this.topicUser = data.user\n        // this.comments = data.comments\n        this.renderMarkdown(this.topic.content)\n        this.flag = true\n      })\n    },\n    handleDelete(id) {\n      deleteTopic(id).then(value => {\n        const { code, message } = value\n        alert(message)\n        if (code === 200) {\n          setTimeout(() => {\n            this.$router.push({ path: \'/\' })\n          }, 500)\n        }\n      })\n    }\n  }\n}\n</script>\n​\n<style>\n#preview {\n  min-height: 300px;\n}\n</style>\n```\n\n', '1360072208037122049', 0, 0, 33, b'0', b'0', 0, '2021-02-13 19:23:16', '2021-02-14 17:46:56', NULL);
INSERT INTO `bms_post` VALUES ('1360553190217420802', '8', 'x\n\n', '1360072208037122049', 0, 0, 0, b'0', b'0', 0, '2021-02-13 19:35:33', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360553257913487361', '8', 'x\n\n', '1360072208037122049', 0, 0, 1, b'0', b'0', 0, '2021-02-13 19:35:49', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360553284643786753', '888', '888\n\n', '1360072208037122049', 0, 0, 1, b'0', b'0', 0, '2021-02-13 19:35:56', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360553982987014146', '111111', '111111111\n\n', '1360072208037122049', 0, 0, 4, b'0', b'0', 0, '2021-02-13 19:38:42', NULL, NULL);
INSERT INTO `bms_post` VALUES ('1360554010119966721', '111111', '111111111\n\n', '1360072208037122049', 0, 0, 0, b'0', b'0', 0, '2021-02-13 19:38:49', NULL, NULL);

-- ----------------------------
-- Table structure for bms_post_tag
-- ----------------------------
DROP TABLE IF EXISTS `bms_post_tag`;
CREATE TABLE `bms_post_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tag_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签ID',
  `post_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '话题ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE,
  INDEX `topic_id`(`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '话题-标签 中间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_post_tag
-- ----------------------------
INSERT INTO `bms_post_tag` VALUES (36, '1332650453377708034', '1332650453142827009');
INSERT INTO `bms_post_tag` VALUES (37, '1332681213568589825', '1332681213400817665');
INSERT INTO `bms_post_tag` VALUES (38, '1332681213631504385', '1332681213400817665');
INSERT INTO `bms_post_tag` VALUES (39, '1332682473218744321', '1332682473151635458');
INSERT INTO `bms_post_tag` VALUES (40, '1332913064463794178', '1332913064396685314');
INSERT INTO `bms_post_tag` VALUES (41, '1332913064530903041', '1332913064396685314');
INSERT INTO `bms_post_tag` VALUES (42, '1333432347107143681', '1333432347031646209');
INSERT INTO `bms_post_tag` VALUES (43, '1333432347107143682', '1333432347031646209');
INSERT INTO `bms_post_tag` VALUES (44, '1333447953697177602', '1333447953558765569');
INSERT INTO `bms_post_tag` VALUES (45, '1332913064463794178', '1333668258587750401');
INSERT INTO `bms_post_tag` VALUES (46, '1333676096320106498', '1333676096156528641');
INSERT INTO `bms_post_tag` VALUES (47, '1333695976742268930', '1333695976536748034');
INSERT INTO `bms_post_tag` VALUES (48, '1334481725519429634', '1334481725322297346');
INSERT INTO `bms_post_tag` VALUES (49, '1333447953697177602', '1335149981733449729');
INSERT INTO `bms_post_tag` VALUES (50, '1349362401597775874', '1349362401438392322');
INSERT INTO `bms_post_tag` VALUES (51, '1349631541306732545', '1349631541260595202');
INSERT INTO `bms_post_tag` VALUES (52, '1360510540852436994', '1360510509969776641');
INSERT INTO `bms_post_tag` VALUES (53, '1360510540852436994', '1360513129052577794');
INSERT INTO `bms_post_tag` VALUES (54, '1360510540852436994', '1360513154323259393');
INSERT INTO `bms_post_tag` VALUES (55, '1360514057117835266', '1360514056601935873');
INSERT INTO `bms_post_tag` VALUES (56, '1360514057117835266', '1360548757836288002');
INSERT INTO `bms_post_tag` VALUES (57, '1360550065179873282', '1360550062604570625');
INSERT INTO `bms_post_tag` VALUES (58, '1360550065179873282', '1360550099032100866');
INSERT INTO `bms_post_tag` VALUES (59, '1360553209645436929', '1360553190217420802');
INSERT INTO `bms_post_tag` VALUES (60, '1360553209645436929', '1360553257913487361');
INSERT INTO `bms_post_tag` VALUES (61, '1360514057117835266', '1360553284643786753');
INSERT INTO `bms_post_tag` VALUES (62, '1360553983758766082', '1360553982987014146');
INSERT INTO `bms_post_tag` VALUES (63, '1360553983758766082', '1360554010119966721');
INSERT INTO `bms_post_tag` VALUES (64, '1360553983758766082', '1360554052729901057');
INSERT INTO `bms_post_tag` VALUES (65, '1360553983758766082', '1360554701039271938');
INSERT INTO `bms_post_tag` VALUES (66, '1360554911756910594', '1360554911123570690');

-- ----------------------------
-- Table structure for bms_promotion
-- ----------------------------
DROP TABLE IF EXISTS `bms_promotion`;
CREATE TABLE `bms_promotion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '广告标题',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '广告链接',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '广告推广表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_promotion
-- ----------------------------
INSERT INTO `bms_promotion` VALUES (1, '开发者头条', 'https://juejin.cn/', '开发者头条');
INSERT INTO `bms_promotion` VALUES (2, '并发编程网', 'https://juejin.cn/', '并发编程网');
INSERT INTO `bms_promotion` VALUES (3, '掘金', 'https://juejin.cn/', '掘金');

-- ----------------------------
-- Table structure for bms_tag
-- ----------------------------
DROP TABLE IF EXISTS `bms_tag`;
CREATE TABLE `bms_tag`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标签',
  `post_count` int(11) NOT NULL DEFAULT 0 COMMENT '关联话题',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_tag
-- ----------------------------
INSERT INTO `bms_tag` VALUES ('1332650453377708034', 'java', 1);
INSERT INTO `bms_tag` VALUES ('1332681213568589825', 'css', 1);
INSERT INTO `bms_tag` VALUES ('1332681213631504385', 'mongodb', 1);
INSERT INTO `bms_tag` VALUES ('1332682473218744321', 'python', 1);
INSERT INTO `bms_tag` VALUES ('1332913064463794178', 'vue', 2);
INSERT INTO `bms_tag` VALUES ('1332913064530903041', 'react', 1);
INSERT INTO `bms_tag` VALUES ('1333432347107143681', 'node', 1);
INSERT INTO `bms_tag` VALUES ('1333432347107143682', 'mysql', 1);
INSERT INTO `bms_tag` VALUES ('1333447953697177602', 'flask', 2);
INSERT INTO `bms_tag` VALUES ('1333676096320106498', 'spring', 1);
INSERT INTO `bms_tag` VALUES ('1333695976742268930', 'django', 1);
INSERT INTO `bms_tag` VALUES ('1334481725519429634', 'security', 1);
INSERT INTO `bms_tag` VALUES ('1349362401597775874', 'tensorflow', 1);
INSERT INTO `bms_tag` VALUES ('1349631541306732545', 'pytorch', 1);
INSERT INTO `bms_tag` VALUES ('1360510540852436994', '8', 3);
INSERT INTO `bms_tag` VALUES ('1360514057117835266', '888', 3);
INSERT INTO `bms_tag` VALUES ('1360550065179873282', 'js', 2);
INSERT INTO `bms_tag` VALUES ('1360553209645436929', 'x', 2);
INSERT INTO `bms_tag` VALUES ('1360553983758766082', '11', 4);
INSERT INTO `bms_tag` VALUES ('1360554911756910594', 'ss', 1);

-- ----------------------------
-- Table structure for bms_tip
-- ----------------------------
DROP TABLE IF EXISTS `bms_tip`;
CREATE TABLE `bms_tip`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
  `type` tinyint(4) NOT NULL COMMENT '1：使用，0：过期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24864 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '每日赠言' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bms_tip
-- ----------------------------
INSERT INTO `bms_tip` VALUES (1, '多锉出快锯，多做长知识。', '佚名', 1);
INSERT INTO `bms_tip` VALUES (2, '未来总留着什么给对它抱有信心的人。', '佚名', 1);
INSERT INTO `bms_tip` VALUES (3, '一个人的智慧不够用，两个人的智慧用不完。', '谚语', 1);
INSERT INTO `bms_tip` VALUES (4, '十个指头按不住十个跳蚤', '傣族', 1);
INSERT INTO `bms_tip` VALUES (5, '言不信者，行不果。', '墨子', 1);
INSERT INTO `bms_tip` VALUES (6, '攀援而登，箕踞而遨，则几数州之土壤，皆在衽席之下。', '柳宗元', 1);
INSERT INTO `bms_tip` VALUES (7, '美德大都包含在良好的习惯之内。', '帕利克', 1);
INSERT INTO `bms_tip` VALUES (8, '人有不及，可以情恕。', '《晋书》', 1);
INSERT INTO `bms_tip` VALUES (9, '明·吴惟顺', '法不传六耳', 1);
INSERT INTO `bms_tip` VALUES (10, '真正的朋友应该说真话，不管那话多么尖锐。', '奥斯特洛夫斯基', 1);
INSERT INTO `bms_tip` VALUES (11, '时间是一切财富中最宝贵的财富。', '德奥弗拉斯多', 1);
INSERT INTO `bms_tip` VALUES (12, '看人下菜碟', '民谚', 1);
INSERT INTO `bms_tip` VALUES (13, '如果不是怕别人反感，女人决不会保持完整的严肃。', '拉罗什福科', 1);
INSERT INTO `bms_tip` VALUES (14, '爱是春暖花开时对你满满的笑意', '佚名', 1);
INSERT INTO `bms_tip` VALUES (15, '希望是坚韧的拐杖，忍耐是旅行袋，携带它们，人可以登上永恒之旅。', '罗素', 1);
INSERT INTO `bms_tip` VALUES (18, '天国般的幸福，存在于对真爱的希望。', '佚名', 1);
INSERT INTO `bms_tip` VALUES (19, '我们现在必须完全保持党的纪律，否则一切都会陷入污泥中。', '马克思', 1);
INSERT INTO `bms_tip` VALUES (20, '在科学上没有平坦的大道，只有不畏劳苦沿着陡峭山路攀登的人，才有希望达到光辉的顶点。', '马克思', 1);
INSERT INTO `bms_tip` VALUES (21, '懒惰的马嫌路远', '蒙古', 1);
INSERT INTO `bms_tip` VALUES (22, '别忘记热水是由冷水烧成的', '非洲', 1);

-- ----------------------------
-- Table structure for ums_user
-- ----------------------------
DROP TABLE IF EXISTS `ums_user`;
CREATE TABLE `ums_user`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
  `username` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `score` int(11) NOT NULL DEFAULT 0 COMMENT '积分',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'token',
  `bio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人简介',
  `active` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否激活，1：是，0：否',
  `status` bit(1) NULL DEFAULT b'1' COMMENT '状态，1：使用，0：停用',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '用户角色',
  `create_time` datetime(0) NOT NULL COMMENT '加入时间',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_name`(`username`) USING BTREE,
  INDEX `user_email`(`email`) USING BTREE,
  INDEX `user_create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ums_user
-- ----------------------------
INSERT INTO `ums_user` VALUES ('1349290158897311745', '王五', '王五', '$2a$10$8qx711TBg/2hxfL7N.sxf.0ROMhR/iuPhQx33IFqGd7PLgt5nGJTO', 'https://s3.ax1x.com/2020/12/01/DfHNo4.jpg', '23456@qq.com', NULL, 2, '', '自由职业者', b'1', b'1', NULL, '2021-01-13 17:40:17', NULL);
INSERT INTO `ums_user` VALUES ('1349618748226658305', 'zhangsan', 'zhangsan', '$2a$10$7K3yYv8sMV5Xsc2facXTcuyDo8JQ4FJHvjZ7qtWYcJdei3Q6Fvqdm', 'https://s3.ax1x.com/2020/12/01/DfHNo4.jpg', '23456@qq.com', NULL, 0, '', '自由职业者', b'1', b'1', NULL, '2021-01-14 15:25:59', NULL);
INSERT INTO `ums_user` VALUES ('1360072208037122049', 'yilimaizi', 'yilimaizi', 'ba2a64d95b6931e4e66976b33bbe0901', 'https://s3.ax1x.com/2020/12/01/DfHNo4.jpg', 'xuzhaoyi@qq.com', NULL, 15, '', '自由职业者', b'1', b'1', NULL, '2021-02-12 11:44:18', NULL);
INSERT INTO `ums_user` VALUES ('1360778252610662401', 'lisi', 'lisi', 'e10adc3949ba59abbe56e057f20f883e', 'https://s3.ax1x.com/2020/12/01/DfHNo4.jpg', 'zhangsan@qq.com', '123456', 0, '', '自由职业者', b'1', b'1', NULL, '2021-02-14 10:29:52', NULL);

SET FOREIGN_KEY_CHECKS = 1;
