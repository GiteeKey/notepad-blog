package com.notepad.blog.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsPost;
import com.notepad.blog.domain.BmsTag;
import com.notepad.blog.mapper.BmsTagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @ClassName BmsTagService
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 17:43
 **/
@Service
public class BmsTagService extends ServiceImpl<BmsTagMapper, BmsTag> {

    @Autowired
    private BmsPostTagService postTagService;

    @Autowired
    private BmsPostService postService;

    public Page<BmsPost> selectTopicsByTagId(Page<BmsPost> topicPage, String id) {

        // 获取关联的话题ID
        Set<String> ids =postTagService.selectPostIdsByTagId(id);
        LambdaQueryWrapper<BmsPost> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(BmsPost::getId, ids);

        return postService.page(topicPage, wrapper);
    }


}
