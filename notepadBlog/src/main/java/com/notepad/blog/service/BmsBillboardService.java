package com.notepad.blog.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsBillboard;
import com.notepad.blog.mapper.BmsBillboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName BmsBillboardService
 * @description: TODO
 * @author: zhaoYi
 * @Date 2021/2/11 17:40
 **/
@Service
public class BmsBillboardService extends ServiceImpl<BmsBillboardMapper, BmsBillboard> {

    @Autowired
    private BmsBillboardMapper billboardMapper;

    public BmsBillboard getNotices() {
        return billboardMapper.getNotices();
    }
}
