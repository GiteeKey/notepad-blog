package com.notepad.blog.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsPromotion;
import com.notepad.blog.mapper.BmsPromotionMapper;
import org.springframework.stereotype.Service;

/**
 * @ClassName BmsPromotionService
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/11 22:25
 **/
@Service
public class BmsPromotionService extends ServiceImpl<BmsPromotionMapper, BmsPromotion> {
}
