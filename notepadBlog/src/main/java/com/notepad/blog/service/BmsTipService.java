package com.notepad.blog.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsTip;
import com.notepad.blog.mapper.BmsTipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName BmsTipService
 * @description: TODO
 * @author: zhaoYi
 * @Date 2021/2/11 21:06
 **/
@Service
public class BmsTipService extends ServiceImpl<BmsTipMapper, BmsTip> {

    @Autowired
    private BmsTipMapper bmsTipMapper;

    public BmsTip getRandomTip() {
        return bmsTipMapper.getRandomTip();
    }
}
