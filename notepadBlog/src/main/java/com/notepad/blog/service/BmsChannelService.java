package com.notepad.blog.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsChannel;
import com.notepad.blog.mapper.BmsChannelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName BmsChannelService
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 10:56
 **/
@Service
public class BmsChannelService extends ServiceImpl<BmsChannelMapper, BmsChannel> {

    public List<BmsChannel> getList() {
        List<BmsChannel> list = this.list();
        return list;
    }
}
