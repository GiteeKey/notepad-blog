package com.notepad.blog.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsPostTag;
import com.notepad.blog.mapper.BmsPostTagMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @ClassName BmsPostTagService
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 16:14
 **/
@Service
public class BmsPostTagService extends ServiceImpl<BmsPostTagMapper, BmsPostTag> {
    public Set<String> selectPostIdsByTagId(String id) {
        return this.baseMapper.getTopicIdsByTagId(id);
    }

    public List<BmsPostTag> selectByTopicId(String id) {
        return null;
    }
}
