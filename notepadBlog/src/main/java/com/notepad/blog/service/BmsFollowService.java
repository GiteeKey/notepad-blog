package com.notepad.blog.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.notepad.blog.domain.BmsFollow;
import com.notepad.blog.mapper.BmsFollowMapper;
import org.springframework.stereotype.Service;

/**
 * @ClassName BmsFollowService
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 20:02
 **/
@Service
public class BmsFollowService extends ServiceImpl<BmsFollowMapper, BmsFollow> {
}
