package com.notepad.blog.domain.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;


@Data
public class CommentDTO implements Serializable {

    private static final long serialVersionUID = -5957433707110390852L;

    @NotBlank(message = "文章不存在")
    private String topic_id;

    /**
     * 内容
     */
    @NotBlank(message = "留言不能为空")
    @Size(min = 2,message = "不能少于2个字符")
    private String content;



}