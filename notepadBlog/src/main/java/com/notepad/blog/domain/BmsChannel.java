package com.notepad.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Builder
@Accessors(chain = true)
@TableName("bms_channel")
public class BmsChannel {

    private String id;
    private String alias;
    private String name;
    private String status;
    private String thumbnail;
    private String weight;

}
