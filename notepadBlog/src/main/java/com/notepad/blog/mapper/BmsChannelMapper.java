package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsChannel;

/**
 * @ClassName BmsChannelMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 10:55
 **/
public interface BmsChannelMapper  extends BaseMapper<BmsChannel> {
}
