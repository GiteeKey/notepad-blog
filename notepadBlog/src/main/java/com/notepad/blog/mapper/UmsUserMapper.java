package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.UmsUser;

/**
 * @ClassName UmsUserMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/11 23:28
 **/
public interface UmsUserMapper  extends BaseMapper<UmsUser> {
}
