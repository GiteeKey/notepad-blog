package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.notepad.blog.domain.BmsPost;
import com.notepad.blog.domain.vo.PostVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName BmsPostMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 12:48
 **/
public interface BmsPostMapper extends BaseMapper<BmsPost> {

    Page<PostVO> selectListAndPage(@Param("page") Page<PostVO> page, @Param("tab") String tab);

    List<BmsPost> getRecommend(String id);

    Page<PostVO> searchByKey(@Param("page") Page<PostVO> page,@Param("keyword") String keyword);
}
