package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsPostTag;

import java.util.List;
import java.util.Set;

/**
 * @ClassName BmsPostTagMapper
 * @description: TODO
 * @author: zhaoYi
 * @Date 2021/2/13 13:11
 **/
public interface BmsPostTagMapper extends BaseMapper<BmsPostTag> {


    public List<BmsPostTag> selectByTostId(String postId);

    Set<String> getTopicIdsByTagId(String id);
}
