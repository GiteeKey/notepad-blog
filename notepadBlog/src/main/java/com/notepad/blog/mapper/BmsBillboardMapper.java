package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsBillboard;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @ClassName BmsBillboardMapper
 * @description: BmsBillboardMapper层接口
 * @author: 一粒麦子
 * @Date 2021/2/11 17:36
 **/
//@Repository  在启动类 @MapperScan("com.notepad.blog.mapper")
public interface BmsBillboardMapper extends BaseMapper<BmsBillboard> {

    BmsBillboard getNotices();
}
