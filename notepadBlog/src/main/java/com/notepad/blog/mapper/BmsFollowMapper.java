package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsFollow;

/**
 * @ClassName BmsFollowMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 19:31
 **/
public interface BmsFollowMapper extends BaseMapper<BmsFollow> {
}
