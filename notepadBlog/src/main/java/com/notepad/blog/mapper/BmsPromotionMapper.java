package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsPromotion;

/**
 * @ClassName BmsPromotionMapper
 * @description: TODO
 * @author: 一粒麦子
 * @Date 2021/2/11 22:24
 **/
public interface BmsPromotionMapper extends BaseMapper<BmsPromotion> {
}
