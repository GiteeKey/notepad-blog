package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsTag;

/**
 * @ClassName BmsTagMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 13:27
 **/
public interface BmsTagMapper extends BaseMapper<BmsTag> {
}
