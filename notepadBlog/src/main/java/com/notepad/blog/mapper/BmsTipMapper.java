package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsTip;

/**
 * @ClassName BmsTipMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/11 20:59
 **/
public interface BmsTipMapper extends BaseMapper<BmsTip> {

    BmsTip getRandomTip();
}
