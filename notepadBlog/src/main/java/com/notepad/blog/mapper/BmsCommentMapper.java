package com.notepad.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.notepad.blog.domain.BmsComment;
import com.notepad.blog.domain.vo.CommentVO;

import java.util.List;

/**
 * @ClassName BmsCommentMapper
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 21:26
 **/
public interface BmsCommentMapper extends BaseMapper<BmsComment> {

    List<CommentVO> getCommentsByTopicID(String topicid);
}
