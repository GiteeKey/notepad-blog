package com.notepad.blog.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.domain.BmsPost;
import com.notepad.blog.domain.UmsUser;
import com.notepad.blog.domain.dto.CreatePostDTO;
import com.notepad.blog.domain.vo.PostVO;
import com.notepad.blog.service.BmsPostService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @ClassName BmsPostController
 * @description: TODO
 * @author: 一粒麦子
 * @Date 2021/2/13 12:41
 **/
@RestController
@RequestMapping("/post")
public class BmsPostController {

    @Autowired
    private BmsPostService postService;

    /**
     * 查询文章列表
     *
     * @param tab
     * @param pageNo
     * @param pageSize
     * @return
     */
    @GetMapping("/list")
    public ApiResult<Page<PostVO>> list(@RequestParam(value = "tab", defaultValue = "latest") String tab,
                                        @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                        @RequestParam(value = "size", defaultValue = "10") Integer pageSize) {
        Page<PostVO> list = postService.getList(pageNo, pageSize, tab);
        return ApiResult.success(list);
    }

    /**
     * 创建文章
     *
     * @return
     */
    @PostMapping("/create")
    public ApiResult createPost(
            @RequestHeader(value = "userName") String userName,
            @RequestBody CreatePostDTO createPostDTO) {
        BmsPost post = postService.createPost(userName, createPostDTO);
        return ApiResult.success(post);
    }

    /**
     * 根据id获取文章详情
     *
     * @return
     */
    @GetMapping
    public ApiResult getPostById(@RequestParam("id") String id) {
        Map<String, Object> map = postService.getPostById(id);
        return ApiResult.success(map);
    }

    /**
     * 随便看看(推荐)
     *
     * @param id
     * @return
     */
    @GetMapping("/recommend")
    public ApiResult getRecommend(@RequestParam("topicId") String id) {
        List<BmsPost> topics = postService.getRecommend(id);
        return ApiResult.success(topics);
    }

    /**
     * 修改文章
     * @param userName
     * @param post
     * @return
     */
    @PostMapping("/update")
    public ApiResult<BmsPost> update(@RequestHeader(value = "userName") String userName, @Valid @RequestBody BmsPost post) {
        post = postService.updateById(userName, post);
        return ApiResult.success(post);
    }

    @DeleteMapping("/delete/{id}")
    public ApiResult<String> delete(@RequestHeader(value = "userName") String userName, @PathVariable("id") String id) {
        postService.deleteById(userName,id);
        return ApiResult.success(null, "删除成功");
    }
}
