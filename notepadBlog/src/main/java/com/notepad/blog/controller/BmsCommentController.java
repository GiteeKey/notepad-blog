package com.notepad.blog.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.common.exception.ApiAsserts;
import com.notepad.blog.domain.BmsComment;
import com.notepad.blog.domain.UmsUser;
import com.notepad.blog.domain.dto.CommentDTO;
import com.notepad.blog.domain.vo.CommentVO;
import com.notepad.blog.service.BmsCommentService;
import com.notepad.blog.service.UmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class BmsCommentController {

    @Resource
    private BmsCommentService commentService;

    @Autowired
    private HttpServletRequest request;

    /**
     * 评论列表
     *
     * @param topicid
     * @return
     */
    @GetMapping("/get_comments")
    public ApiResult<List<CommentVO>> getCommentsByTopicID(@RequestParam(value = "topicid", defaultValue = "1") String topicid) {
        List<CommentVO> lstBmsComment = commentService.getCommentsByTopicID(topicid);
        return ApiResult.success(lstBmsComment);
    }

    /**
     * 添加留言
     *
     * @param userName
     * @param dto
     * @return
     */
    @PostMapping("/add_comment")
    public ApiResult<BmsComment> addComment(@Validated @RequestHeader(value = "userName") String userName,
                                            @RequestBody CommentDTO dto) {
        BmsComment comment = commentService.addComment(dto, userName);
        return ApiResult.success(comment);
    }
}