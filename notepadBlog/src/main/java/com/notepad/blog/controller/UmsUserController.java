package com.notepad.blog.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.config.jwt.JwtUtil;
import com.notepad.blog.domain.BmsPost;
import com.notepad.blog.domain.UmsUser;
import com.notepad.blog.domain.dto.LoginDTO;
import com.notepad.blog.domain.dto.RegisterDTO;
import com.notepad.blog.service.BmsPostService;
import com.notepad.blog.service.UmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UmsUserController
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/11 23:29
 **/
@RestController
@RequestMapping("/auth/user")
public class UmsUserController{

    @Autowired
    private UmsUserService umsUserService;

    @Autowired
    private BmsPostService postService;

    /**
     * 注册
     *
     * @param registerDTO 接收参数
     * @return
     */
    @PostMapping("/register")
    private ApiResult register(@RequestBody RegisterDTO registerDTO) {
        Boolean register = umsUserService.register(registerDTO);
        return ApiResult.success(register);
    }

    @PostMapping("/login")
    public ApiResult login(@Valid @RequestBody LoginDTO loginDTO) {
        Map<String, String> map = umsUserService.login(loginDTO);
        return ApiResult.success(map, "登录成功");
    }

    @GetMapping("/info")
    public ApiResult loginUserInfo(@RequestHeader(value = "userName") String userName) {
        UmsUser umsUser = umsUserService.getOne(
                new LambdaQueryWrapper<UmsUser>().eq(UmsUser::getUsername, userName)
        );
        return ApiResult.success(umsUser);
    }

    @GetMapping("/logout")
    public ApiResult logout() {
        return ApiResult.success(null, "注销成功");
    }

    @GetMapping("/{username}")
    public ApiResult<Map<String, Object>> getUserByName(@PathVariable("username") String userName,
                                                        @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                        @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Map<String, Object> map = new HashMap<>(16);
        UmsUser user = umsUserService.getOne(
                new LambdaQueryWrapper<UmsUser>().eq(UmsUser::getUsername, userName)
        );
        Assert.notNull(user, "用户不存在");
        Page<BmsPost> page = postService.page(new Page<>(pageNo, size),
                new LambdaQueryWrapper<BmsPost>().eq(BmsPost::getUserId, user.getId()));
        map.put("user", user);
        map.put("topics", page);
        return ApiResult.success(map);
    }

    /**
     * 设置中心跟新用户信息
     * @param umsUser
     * @return
     */
    @PostMapping("/update")
    public ApiResult<UmsUser> updateUser(@RequestBody UmsUser umsUser) {
        umsUserService.updateById(umsUser);
        return ApiResult.success(umsUser);
    }
}
