package com.notepad.blog.controller;

import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.domain.BmsBillboard;
import com.notepad.blog.service.BmsBillboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName BmsBillboardController
 * @description: 公告
 * @author: 一粒麦子
 * @Date 2021/2/11 17:36
 **/
@RestController
@RequestMapping("/billboard")
public class BmsBillboardController {

    @Autowired
    private BmsBillboardService bmsBillboardService;

    @GetMapping("/show")
    public ApiResult getNotices() {
        BmsBillboard billboard = bmsBillboardService.getNotices();
        return ApiResult.success(billboard);
    }
}
