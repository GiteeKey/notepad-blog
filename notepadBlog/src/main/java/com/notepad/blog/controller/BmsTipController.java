package com.notepad.blog.controller;

import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.domain.BmsTip;
import com.notepad.blog.service.BmsTipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName BmsTip
 * @description: TODO
 * @author: zhaoYi
 * @Date 2021/2/11 21:05
 **/
@RestController
@RequestMapping("tip")
public class BmsTipController {

    @Autowired
    private BmsTipService tipService;

    @GetMapping("/today")
    public ApiResult getRandomTip() {
        BmsTip tip = tipService.getRandomTip();
        return ApiResult.success(tip);
    }
}
