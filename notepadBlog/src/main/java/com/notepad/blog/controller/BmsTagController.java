package com.notepad.blog.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.PageList;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.domain.BmsPost;
import com.notepad.blog.domain.BmsTag;
import com.notepad.blog.service.BmsTagService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tag")
public class BmsTagController {

    @Resource
    private BmsTagService tagService;

    @GetMapping("/{name}")
    public ApiResult<Map<String, Object>> getTopicsByTag(
            @PathVariable("name") String tagName,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {

        Map<String, Object> map = new HashMap<>(16);

        LambdaQueryWrapper<BmsTag> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BmsTag::getName, tagName);
        BmsTag one = tagService.getOne(wrapper);
        Assert.notNull(one, "话题不存在，或已被管理员删除");
        Page<BmsPost> topics = tagService.selectTopicsByTagId(new Page<>(page, size), one.getId());
        // 其他热门标签
        Page<BmsTag> hotTags = tagService.page(new Page<>(1, 100),
                new LambdaQueryWrapper<BmsTag>()
                        .notIn(BmsTag::getName, tagName)
                        .orderByDesc(BmsTag::getPostCount));

        map.put("topics", topics);
        map.put("hotTags", hotTags);

        return ApiResult.success(map);
    }

    /**
     * 标签页查询
     *
     * @param tagName
     * @param page
     * @param size
     * @return
     */
    @GetMapping("list/{name}")
    public ApiResult<Map<String, Object>> getTagList(
            @PathVariable("name") String tagName,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {

        Map<String, Object> map = new HashMap<>(16);

        if ("undefined".equals(tagName)) {
            // 其他标签
            Page<BmsTag> hotTags = tagService.page(new Page<>(1, 100),
                    new LambdaQueryWrapper<BmsTag>()
                            .notIn(BmsTag::getName, tagName)
                            .orderByDesc(BmsTag::getPostCount));
            map.put("hotTags", hotTags);
        } else {
            LambdaQueryWrapper<BmsTag> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BmsTag::getName, tagName);
            BmsTag one = tagService.getOne(wrapper);
            Assert.notNull(one, "话题不存在，或已被管理员删除");
            Page<BmsPost> topics = tagService.selectTopicsByTagId(new Page<>(page, size), one.getId());
            // 其他热门标签
            Page<BmsTag> hotTags = tagService.page(new Page<>(1, 100),
                    new LambdaQueryWrapper<BmsTag>()
                            .notIn(BmsTag::getName, tagName)
                            .orderByDesc(BmsTag::getPostCount));

            map.put("topics", topics);
            map.put("hotTags", hotTags);
        }
        return ApiResult.success(map);
    }

}