package com.notepad.blog.controller;

import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.common.exception.ApiAsserts;
import com.notepad.blog.domain.BmsPromotion;
import com.notepad.blog.mapper.BmsBillboardMapper;
import com.notepad.blog.mapper.BmsPromotionMapper;
import com.notepad.blog.service.BmsBillboardService;
import com.notepad.blog.service.BmsPromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName BmsPromotionController
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/11 22:23
 **/
@RestController
@RequestMapping("/promotion")
public class BmsPromotionController {

    @Autowired
    private BmsPromotionService promotionService;

    @GetMapping("/list")
    public ApiResult getPromotionList(){
        List<BmsPromotion> list = promotionService.list();
        return ApiResult.success(list);
    }

}
