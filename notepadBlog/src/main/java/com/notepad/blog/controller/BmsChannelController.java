package com.notepad.blog.controller;

import com.notepad.blog.common.api.ApiResult;
import com.notepad.blog.domain.BmsChannel;
import com.notepad.blog.service.BmsChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName BmsChannelController
 * @description:
 * @author: 一粒麦子
 * @Date 2021/2/13 10:57
 **/
@RestController
@RequestMapping("/channel")
public class BmsChannelController {

    @Autowired
    private BmsChannelService channelService;

    @GetMapping("/list")
    public ApiResult getList() {
        List<BmsChannel> channelList = channelService.getList();
        return ApiResult.success(channelList);
    }
}
